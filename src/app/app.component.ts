import { Component } from '@angular/core';

@Component({
  selector: 'cgk-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'cgk';
}
